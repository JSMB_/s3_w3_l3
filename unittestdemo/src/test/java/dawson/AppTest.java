package dawson;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.*;


import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{

    App app = new App();
    /**
     * Rigorous Test :-)
     */
    @Test
    public void testEcho() {
        assertEquals( "confirming the method echo's return is correct", app.echo(5), 5 );
    }

    @Test
    public void testOneMore() {
        assertEquals( "confirming the method oneMore's return is correct", app.oneMore(5), 6 );
    }
}
